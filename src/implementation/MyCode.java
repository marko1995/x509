package implementation;

import code.GuiException;
import gui.Constants;
import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.math.BigInteger;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.Security;
import java.security.SignatureException;
import java.security.UnrecoverableKeyException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
//import org.bouncycastle.jcajce.provider.asymmetric.x509.CertificateFactory;
import java.security.cert.CertificateFactory;
import java.security.cert.X509CRL;
import java.security.cert.X509Certificate;
import java.security.interfaces.DSAPublicKey;
import java.security.interfaces.ECPrivateKey;
import java.security.interfaces.ECPublicKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.ECParameterSpec;
import java.security.spec.EllipticCurve;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.print.attribute.standard.Finishings;
import javax.tools.FileObject;
import org.bouncycastle.asn1.ASN1InputStream;
import org.bouncycastle.asn1.ASN1Integer;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import org.bouncycastle.asn1.ASN1Primitive;
import org.bouncycastle.asn1.ASN1Sequence;
import org.bouncycastle.asn1.DEROctetString;
import org.bouncycastle.asn1.DERSequence;
import org.bouncycastle.asn1.pkcs.PKCSObjectIdentifiers;
import org.bouncycastle.asn1.x500.X500Name;
import org.bouncycastle.asn1.x509.AlgorithmIdentifier;
import org.bouncycastle.asn1.x509.CertificatePolicies;
import org.bouncycastle.asn1.x509.Extension;
import org.bouncycastle.asn1.x509.GeneralName;
import org.bouncycastle.asn1.x509.GeneralNames;
import org.bouncycastle.asn1.x509.PolicyInformation;
import org.bouncycastle.asn1.x509.PolicyQualifierId;
import org.bouncycastle.asn1.x509.PolicyQualifierInfo;
import org.bouncycastle.cert.X509CertificateHolder;
import org.bouncycastle.cert.jcajce.JcaX509CertificateConverter;
import org.bouncycastle.cert.jcajce.JcaX509CertificateHolder;
import org.bouncycastle.cert.jcajce.JcaX509v3CertificateBuilder;
import org.bouncycastle.cms.CMSException;
import org.bouncycastle.cms.CMSProcessableByteArray;
import org.bouncycastle.cms.CMSSignedData;
import org.bouncycastle.cms.CMSSignedDataGenerator;
import org.bouncycastle.cms.CMSTypedData;
import org.bouncycastle.cms.SignerInfoGeneratorBuilder;
import org.bouncycastle.jce.ECNamedCurveTable;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.asn1.x509.Attribute;
import org.bouncycastle.asn1.x509.ExtensionsGenerator;
import org.bouncycastle.asn1.x509.SubjectPublicKeyInfo;
import org.bouncycastle.cert.jcajce.JcaCertStore;
import org.bouncycastle.crypto.Digest;
import org.bouncycastle.jce.spec.ECNamedCurveParameterSpec;
import org.bouncycastle.jce.spec.ECNamedCurveSpec;
import org.bouncycastle.openssl.jcajce.JcaPEMWriter;
import org.bouncycastle.operator.ContentSigner;
import org.bouncycastle.operator.OperatorCreationException;
import org.bouncycastle.operator.jcajce.JcaContentSignerBuilder;
import org.bouncycastle.operator.jcajce.JcaDigestCalculatorProviderBuilder;
import org.bouncycastle.pkcs.PKCS10CertificationRequest;
import org.bouncycastle.pkcs.PKCS10CertificationRequestBuilder;
import org.bouncycastle.pkcs.jcajce.JcaPKCS10CertificationRequest;
import org.bouncycastle.pkcs.jcajce.JcaPKCS10CertificationRequestBuilder;
import org.bouncycastle.util.encoders.Base64;
import org.bouncycastle.util.io.pem.PemObject;
import org.bouncycastle.x509.extension.X509ExtensionUtil;
import x509.v3.CodeV3;

//https://www.ietf.org/rfc/rfc5280.txt
public class MyCode extends CodeV3 {

    private static final String KSTORE_FILE = "kstore.p12";
    private static final String ECDSA = "ECDSA";
    private static final String KEY_STORE_PKCS12 = "PKCS12";
    private static final String X509 = "X509";
    private static final String RSA = "RSA";
    private static final String DSA = "DSA";
    private static final String EC = "EC";
    private static final char[] KEY_STORE_PASSWORD = "password".toCharArray();

    private JcaPKCS10CertificationRequest csr = null;

    public MyCode(boolean[] algorithm_conf, boolean[] extensions_conf, boolean extensions_rules) throws GuiException {
        super(algorithm_conf, extensions_conf, extensions_rules);

    }

    @Override
    public Enumeration<String> loadLocalKeystore() {
        Security.addProvider(new BouncyCastleProvider());
        //dohvatanje instance...baca izuzetak i vraca null ako ne uspe
        try {
            System.out.println("loadLocalKeystore");

            //dohvatanje instance
            KeyStore keyStore = KeyStore.getInstance(KEY_STORE_PKCS12, BouncyCastleProvider.PROVIDER_NAME);

            // cita iz fajla sa odredjenim passwordom
            // u slucaju greske, ucitava null null
            try (FileInputStream fis = new FileInputStream(new File(KSTORE_FILE))) {
                keyStore.load(fis, KEY_STORE_PASSWORD);
            } catch (IOException | NoSuchAlgorithmException | CertificateException ex) {
                Logger.getLogger(MyCode.class.getName()).log(Level.SEVERE, null, ex);
                keyStore.load(null, null);
            }

            //vraca sve sacuvane aliase
            return keyStore.aliases();

        } catch (KeyStoreException | IOException | NoSuchAlgorithmException | CertificateException | NoSuchProviderException ex) {
            Logger.getLogger(MyCode.class.getName()).log(Level.SEVERE, null, ex);

        }
        return null;

    }

    @Override
    public void resetLocalKeystore() {
        System.out.println("resetLocalKeystore");

        //brise fajl sa svim sacuvanim kljucevima
        File file = new File(KSTORE_FILE);
        file.delete();

    }

    @Override
    public boolean removeKeypair(String string) {
        System.out.println("removeKeypair " + string);

        //dohvatanje instance, ako ne uspe, vraca false
        try {
            KeyStore keyStore = KeyStore.getInstance(KEY_STORE_PKCS12, BouncyCastleProvider.PROVIDER_NAME);

            try (FileInputStream fis1 = new FileInputStream(new File(KSTORE_FILE))) {

                //ucitavanje sacuvanih kljuceva iz fajla, ako dodje do greske, loaduje se null null
                keyStore.load(fis1, KEY_STORE_PASSWORD);
            } catch (IOException | NoSuchAlgorithmException | CertificateException ex) {
                Logger.getLogger(MyCode.class.getName()).log(Level.SEVERE, null, ex);

                try {
                    //u slucaju greske loaduje je null null
                    keyStore.load(null, null);
                } catch (IOException | NoSuchAlgorithmException | CertificateException ex1) {
                    Logger.getLogger(MyCode.class.getName()).log(Level.SEVERE, null, ex1);
                }
            }

            // brisanje kljuca i storovane u fajl
            try (FileOutputStream fos = new FileOutputStream(new File(KSTORE_FILE))) {

                //brise kljuc
                keyStore.deleteEntry(string);

                //storuje ostale kljuceve
                keyStore.store(fos, KEY_STORE_PASSWORD);

                // ako uspe, vraca true, u suprotnom vraca false
                return true;
            } catch (KeyStoreException | IOException | NoSuchAlgorithmException | CertificateException ex) {
                Logger.getLogger(MyCode.class.getName()).log(Level.SEVERE, null, ex);
                return false;
            }
        } catch (KeyStoreException | NoSuchProviderException ex) {
            Logger.getLogger(MyCode.class.getName()).log(Level.SEVERE, null, ex);

        }
        return false;
    }

    @Override
    public boolean importKeypair(String string, String string1, String string2) {
        System.out.println("importKeypair " + string + " " + string1 + " " + string2);

        KeyStore keyStore;

        //dohvatanje instance
        try {
            keyStore = KeyStore.getInstance(KEY_STORE_PKCS12, BouncyCastleProvider.PROVIDER_NAME);

            //kreira inputstream do kljuca koji treba da se importuje 
            try (FileInputStream fis = new FileInputStream(new File(string1))) {

                //ucitavanje kljuca koji treba importovati
                keyStore.load(fis, string2.toCharArray());

                //dohvatanje kljuca i niz sertifikata
                Key key = keyStore.getKey(string, string2.toCharArray());
                Certificate[] certs = keyStore.getCertificateChain(string);

                //dohvatanje nove instance
                keyStore = KeyStore.getInstance(KEY_STORE_PKCS12, BouncyCastleProvider.PROVIDER_NAME);

                //otvaranje inputstreama do fajla sa kog treba da se ucitaju kljucevi
                try (FileInputStream fis1 = new FileInputStream(new File(KSTORE_FILE))) {

                    //ucitavanje sacuvanih kljuceva iz fajla, ako dodje do greske, loaduje se null null
                    keyStore.load(fis1, KEY_STORE_PASSWORD);
                } catch (IOException | NoSuchAlgorithmException | CertificateException ex) {
                    Logger.getLogger(MyCode.class.getName()).log(Level.SEVERE, null, ex);

                    //u slucaju greske loaduje je null null
                    keyStore.load(null, null);
                }

                //provera da li alias vec postoji... ako postoji vraca false 
                if (keyStore.isKeyEntry(string)) {
                    return false;
                }

                //smesta se kljuc u keystore
                keyStore.setKeyEntry(string, key, KEY_STORE_PASSWORD, certs);

                //otvaranje outputstreama za cuvanje svih ucitanih kljuceva
                try (FileOutputStream fos = new FileOutputStream(new File(KSTORE_FILE))) {

                    //cuvanje svih ucitanih fajlova
                    keyStore.store(fos, KEY_STORE_PASSWORD);
                } catch (KeyStoreException | NoSuchAlgorithmException | CertificateException | IOException ex) {
                    Logger.getLogger(MyCode.class.getName()).log(Level.SEVERE, null, ex);
                    return false;
                }

                //ako je sve proslo kako treba, vratice true, u suprotnom vraca false
                return true;
            } catch (IOException | NoSuchAlgorithmException | CertificateException | UnrecoverableKeyException ex) {
                Logger.getLogger(MyCode.class.getName()).log(Level.SEVERE, null, ex);

            }

        } catch (KeyStoreException | NoSuchProviderException ex) {
            Logger.getLogger(MyCode.class.getName()).log(Level.SEVERE, null, ex);

        }
        return false;
    }

    @Override
    public int loadKeypair(String string) {
        System.out.println("loadKeypair " + string);
        try (FileInputStream fis = new FileInputStream(new File(KSTORE_FILE))) {

            //dohvatanje instance
            KeyStore keyStore = KeyStore.getInstance(KEY_STORE_PKCS12, BouncyCastleProvider.PROVIDER_NAME);

            //citanje iz fajla sa svim kljucevima
            keyStore.load(fis, KEY_STORE_PASSWORD);

            //dohvatanje sertifikata sa aliasom string
            X509Certificate certificate = (X509Certificate) keyStore.getCertificate(string);

            //provera da li objekat certificate postoji, ako ne postoji vraca -1
            if (certificate == null) {
                return -1;
            }

            //test ispis
            System.out.println(certificate.toString());

            //holder za sertifikat, bolje parsira 
            JcaX509CertificateHolder certificateHolder = new JcaX509CertificateHolder(certificate);

            //test ispis za sig algoritam 
            System.out.println("sigalgname: " + certificate.getSigAlgName());

            //setovanje verzije x509 na gui
            access.setVersion(certificate.getVersion() - 1);

            //ispituje da li je 
            if (certificate.getSerialNumber() != null) {
                access.setSerialNumber(certificate.getSerialNumber().toString());
            }

            //setuje datum notbefore i notafter na gui
            access.setNotBefore(certificate.getNotBefore());
            access.setNotAfter(certificate.getNotAfter());
	
		String alg = certificate.getSigAlgName().replace("WITH", "with");
		
            //provera da li postoji issuer
            if (certificateHolder.getIssuer() != null) {

                //test ispis podataka o issuer
                System.out.println("setIssuer: " + certificateHolder.getIssuer().toString());

                //setovane svih polja za issuer-a na gui (CA info, donji okvir) ako postoji issuer
                access.setIssuer(certificateHolder.getIssuer().toString());

                //setuje naziv algoritma na gui sa kojim je sertifikat potpisan
                access.setIssuerSignatureAlgorithm(alg);
            }

            //provera da li postoji subject
            if (certificateHolder.getSubject() != null) {

                //test ispis za subject-a
                System.out.println("setSubject: " + certificateHolder.getSubject().toString());

                //setovane svih polja za subject-a na gui (Subject info, gornji okir) ako postoji subject
                access.setSubject(certificateHolder.getSubject().toString());

            }

            PublicKey publicKey = certificate.getPublicKey();
            
//            access.setSubjectSignatureAlgorithm(certificate.getSigAlgName());
//            //provera da li postoji publickey i njegov algoritam, ako ne postoji, setuje se sa prazim poljem
//            if (publicKey != null && publicKey.getAlgorithm() != null) {
//
//                //test ispis getPublicKey
//                System.out.println("getPublicKey: " + publicKey);
//
//                //test ispis getPublicKeyAlgorithm
//                System.out.println("getPublicKeyAlgorithm: " + publicKey.getAlgorithm());
//
//                //setuje radio button na gui 
            
//
            if (publicKey.getAlgorithm().equals("RSA")) {
                RSAPublicKey rsaPublicKey = (RSAPublicKey) publicKey;
                access.setPublicKeyAlgorithm(rsaPublicKey.getAlgorithm());
                
                //setuje na gui posle radion buttona parametre vezane za sertifikat
                access.setPublicKeyDigestAlgorithm(alg);
                access.setPublicKeyParameter("" + rsaPublicKey.getModulus().bitLength());
                access.setSubjectSignatureAlgorithm(alg);
            } else if (publicKey.getAlgorithm().equals("EC")) {
                ECPublicKey ecPublicKey = (ECPublicKey) publicKey;
                access.setPublicKeyAlgorithm(ecPublicKey.getAlgorithm());
                ECNamedCurveSpec ecps = (ECNamedCurveSpec) ecPublicKey.getParams();

                //access.setPublicKeyParameter();
                /////////////////////////////////////////////////////////////////////////////
                //moguca greska!!!
                String curve = ecps.getName();
                String set;
                if (curve.equals("prime256v1")) {
                    set = "X9.62";
                } else if (curve.contains("sec")) {
                    set = "SEC";
                } else {
                    set = "NIST";
                }
                
                
                
                //setuje na gui posle radion buttona parametre vezane za sertifikat
                //System.out.println("/" + certificate.getSigAlgName() + "/");
                //access.setPublicKeyParameter(certificate.getSigAlgName());
                access.setPublicKeyParameter(set);
                access.setPublicKeyDigestAlgorithm(alg);
                access.setSubjectSignatureAlgorithm(alg);
                access.setPublicKeyECCurve(curve);

                /////////////////////////////////////////////////////////////////////////////
            }

            if (certificate.getCriticalExtensionOIDs() != null || certificate.getNonCriticalExtensionOIDs() != null) {
                if ((certificate.getCriticalExtensionOIDs() != null && certificate.getCriticalExtensionOIDs().contains(Extension.subjectAlternativeName.toString())
                        || certificate.getNonCriticalExtensionOIDs() != null && certificate.getNonCriticalExtensionOIDs().contains(Extension.subjectAlternativeName.toString()))
                        && certificate.getSubjectAlternativeNames() != null) {

                    String altName = "";
                    for (List<?> list : certificate.getSubjectAlternativeNames()) {
                        String name = list.get(1).toString();
                        switch (Integer.parseInt(list.get(0).toString())) {
                            case GeneralName.otherName:
                                altName += "otherName=" + name;
                                break;
                            case GeneralName.rfc822Name:
                                altName += "rfc822Name=" + name;
                                break;
                            case GeneralName.dNSName:
                                altName += "dNSName=" + name;
                                break;
                            case GeneralName.x400Address:
                                altName += "x400Address=" + name;
                                break;
                            case GeneralName.directoryName:
                                altName += "directoryName=" + name;
                                break;
                            case GeneralName.ediPartyName:
                                altName += "ediPartyName=" + name;
                                break;
                            case GeneralName.uniformResourceIdentifier:
                                altName += "uniformResourceIdentifier=" + name;
                                break;
                            case GeneralName.iPAddress:
                                altName += "iPAddress=" + name;
                                break;
                            case GeneralName.registeredID:
                                altName += "registeredID=" + name;
                                break;

                        }
                        altName += ",";
                    }
                    if (altName.length() > 0) {
                        altName = altName.substring(0, altName.length() - 1);
                    }

                    if (certificate.getCriticalExtensionOIDs() != null && certificate.getCriticalExtensionOIDs().contains(Extension.subjectAlternativeName.toString())) {
                        access.setCritical(Constants.SAN, true);
                    } else {
                        access.setCritical(Constants.SAN, false);
                    }

                    access.setAlternativeName(Constants.SAN, altName);

                }

                if (certificate.getCriticalExtensionOIDs() != null && certificate.getCriticalExtensionOIDs().contains(Extension.inhibitAnyPolicy.toString())
                        || certificate.getNonCriticalExtensionOIDs() != null && certificate.getNonCriticalExtensionOIDs().contains(Extension.inhibitAnyPolicy.toString())) {

                    byte[] data = certificate.getExtensionValue(Extension.inhibitAnyPolicy.getId());

                    if (data == null) {
                        access.setInhibitAnyPolicy(false);
                    } else {
                        try (InputStream is = new ByteArrayInputStream(data);
                                ASN1InputStream asnis = new ASN1InputStream(is)) {

                            ASN1Primitive asnp = asnis.readObject();

                            DEROctetString deros = (DEROctetString) asnp;

                            try (InputStream is1 = new ByteArrayInputStream(deros.getOctets());
                                    ASN1InputStream asnis1 = new ASN1InputStream(is1)) {
                                ASN1Primitive asnp1 = asnis1.readObject();
                                access.setInhibitAnyPolicy(true);
                                access.setSkipCerts(asnp1.toString());
                            }
                        }

                    }

                    if (certificate.getCriticalExtensionOIDs() != null && certificate.getCriticalExtensionOIDs().contains(Extension.inhibitAnyPolicy.toString())) {
                        access.setCritical(Constants.IAP, true);
                    } else {
                        access.setCritical(Constants.IAP, false);
                    }

                }
                if (certificate.getCriticalExtensionOIDs() != null && certificate.getCriticalExtensionOIDs().contains(Extension.certificatePolicies.toString())
                        || certificate.getNonCriticalExtensionOIDs() != null && certificate.getNonCriticalExtensionOIDs().contains(Extension.certificatePolicies.toString())) {

                    byte[] policyBytes = certificate.getExtensionValue(Extension.certificatePolicies.getId());

                    if (policyBytes != null) {

//                        StringBuilder builder = new StringBuilder(new String(policyBytes));
//                        while (true) {
//                            if (!(Character.isAlphabetic(builder.charAt(0)) || Character.isDigit(builder.charAt(0)))) {
//                                builder.deleteCharAt(0);
//                            } else {
//                                break;
//                            }
//                        }
//                        String policies = builder.toString();
//                        access.setAnyPolicy(true);
//                        access.setCpsUri(policies);
//                        access.setAnyPolicy(true);
//                        access.setCpsUri(policies);
                        CertificatePolicies policies = CertificatePolicies.getInstance(X509ExtensionUtil.fromExtensionValue(policyBytes));
                        PolicyInformation[] policyInformations = policies.getPolicyInformation();
                        for (PolicyInformation policyInformation : policyInformations) {
                            ASN1Sequence policyQualifiers = (ASN1Sequence) policyInformation.getPolicyQualifiers().getObjectAt(0);
                            System.out.println(policyQualifiers.getObjectAt(1)); 
                            access.setAnyPolicy(true);
                            access.setCpsUri(policyQualifiers.getObjectAt(1).toString());
                            break;
                        }

//                        try (InputStream is = new ByteArrayInputStream(policyBytes);
//                                ASN1InputStream asnis = new ASN1InputStream(is)) {
//                            ASN1Primitive asnp = asnis.readObject();
//                            DEROctetString deros = (DEROctetString) asnp;
//
//                            try (InputStream is1 = new ByteArrayInputStream(deros.getOctets());
//                                    ASN1InputStream asnis1 = new ASN1InputStream(is1)) {
//                                ASN1Primitive asnp1 = asnis1.readObject();
//                                CertificatePolicies policies = CertificatePolicies.getInstance(asnp1);
//                                PolicyInformation[] policyInformation = policies.getPolicyInformation();
//                                for (PolicyInformation pInfo : policyInformation) {
//                                    ASN1Sequence policyQualifiers = (ASN1Sequence) pInfo.getPolicyQualifiers().getObjectAt(0);
//                                    //System.out.println(policyQualifiers.getObjectAt(1)); // aaa.bbb
//                                    access.setAnyPolicy(true);
//                                    access.setCpsUri(policyQualifiers.getObjectAt(1).toString());
//                                }
//                            }
//                        } catch (Exception ex) {
//                            Logger.getLogger(MyCode.class.getName()).log(Level.SEVERE, null, ex);
//                        }
//                        
//
//                    Extensions extensions = certificateHolder.getExtensions();
//                    CertificatePolicies policies = CertificatePolicies.fromExtensions(extensions);
//                    PolicyInformation[] policyInformation = policies.getPolicyInformation();
//                    if (policyInformation != null) {
//                        for (PolicyInformation pInfo : policyInformation) {
//                            ASN1Sequence policyQualifiers = (ASN1Sequence) pInfo.getPolicyQualifiers().getObjectAt(0);
//                            //System.out.println(policyQualifiers.getObjectAt(1)); // aaa.bbb
//                            access.setAnyPolicy(true);
//                            access.setCpsUri(policyQualifiers.getObjectAt(1).toString());
//                        }
                    } else {
                        access.setAnyPolicy(false);
                    }

                    if (certificate.getCriticalExtensionOIDs() != null && certificate.getCriticalExtensionOIDs().contains(Extension.certificatePolicies.toString())) {
                        access.setCritical(Constants.CP, true);
                    } else {
                        access.setCritical(Constants.CP, false);
                    }

                }

            }

            if (keyStore.isCertificateEntry(string)) {
                return 2;
            }
            if (keyStore.isKeyEntry(string)) {
                Certificate chain[] = keyStore.getCertificateChain(string);

//                if (chain.length != 1 || ((lastInChain.getKeyUsage() != null && lastInChain.getKeyUsage()[5] == true) && lastInChain.getBasicConstraints() != -1)) {
//                    return 1;
//                }
                try {
                    X509Certificate lastInChain = (X509Certificate) chain[0];
                    PublicKey certPublicKey = lastInChain.getPublicKey();
                    lastInChain.verify(certPublicKey);
                } catch (InvalidKeyException | SignatureException ex) {
                    Logger.getLogger(MyCode.class.getName()).log(Level.SEVERE, null, ex);
                    return 1;
                }

            }
            return 0;
        } catch (KeyStoreException | NoSuchAlgorithmException | CertificateException | IOException | NoSuchProviderException ex) {
            Logger.getLogger(MyCode.class.getName()).log(Level.SEVERE, null, ex);
        }
        return -1;
    }

    @Override
    public boolean exportKeypair(String string, String string1, String string2) {
        System.out.println("exportKeypair " + string + " " + string1 + " " + string2);

        try {
            KeyStore keyStore = KeyStore.getInstance(KEY_STORE_PKCS12, BouncyCastleProvider.PROVIDER_NAME);
            try (InputStream is = new FileInputStream(new File(KSTORE_FILE))) {
                keyStore.load(is, KEY_STORE_PASSWORD);
            } catch (Exception ex) {
                Logger.getLogger(MyCode.class.getName()).log(Level.SEVERE, null, ex);
                return false;
            }

            

            Key key = keyStore.getKey(string, KEY_STORE_PASSWORD);
            Certificate[] certificates = keyStore.getCertificateChain(string);
            keyStore = KeyStore.getInstance(KEY_STORE_PKCS12, BouncyCastleProvider.PROVIDER_NAME);
            try (InputStream is1 = new FileInputStream(string1)) {

                keyStore.load(is1, string2.toCharArray());

            } catch (IOException | CertificateException ex) {
                Logger.getLogger(MyCode.class.getName()).log(Level.SEVERE, null, ex);
                keyStore.load(null, null);
            }

            keyStore.setKeyEntry(string, key, string2.toCharArray(), certificates);

            try (OutputStream os = new FileOutputStream(new File(string1))) {
                keyStore.store(os, string2.toCharArray());
            }

            return true;

        } catch (KeyStoreException | NoSuchAlgorithmException | UnrecoverableKeyException | IOException | CertificateException | NoSuchProviderException ex) {
            Logger.getLogger(MyCode.class.getName()).log(Level.SEVERE, null, ex);
        }

        return false;
    }

    //https://stackoverflow.com/questions/44186758/set-get-x509-certificatepolicies-extension-when-importing-der-pem-in-java
    @Override
    public boolean saveKeypair(String string) {
        System.out.println("saveKeypair " + string);

        try {
            KeyStore keyStore = KeyStore.getInstance(KEY_STORE_PKCS12, BouncyCastleProvider.PROVIDER_NAME);

            try (InputStream is = new FileInputStream(new File(KSTORE_FILE))) {
                keyStore.load(is, KEY_STORE_PASSWORD);
            } catch (IOException ex) {
                Logger.getLogger(MyCode.class.getName()).log(Level.SEVERE, null, ex);
                keyStore.load(null, null);
            }

            if (keyStore.isKeyEntry(string)) {
                return false;
            }

            System.out.println(access.getPublicKeyECCurve() + " " + access.getPublicKeyAlgorithm() + " " + access.getPublicKeyParameter() + " " + access.getPublicKeyDigestAlgorithm());

            String curve = access.getPublicKeyECCurve();
            ECNamedCurveParameterSpec ecSpec = ECNamedCurveTable.getParameterSpec(curve);
            KeyPairGenerator g = KeyPairGenerator.getInstance(MyCode.ECDSA, BouncyCastleProvider.PROVIDER_NAME);

            g.initialize(ecSpec, new SecureRandom());

            KeyPair keyPair = g.genKeyPair();

            ECPublicKey cPublicKey = (ECPublicKey) keyPair.getPublic();
            ECPrivateKey cPrivateKey = (ECPrivateKey) keyPair.getPrivate();

            System.out.println("" + access.getIssuer());

            X500Name subject = new X500Name(access.getSubject());

            Date notBefore = access.getNotBefore();
            Date notAfter = access.getNotAfter();

            JcaX509v3CertificateBuilder builder = new JcaX509v3CertificateBuilder(subject, new BigInteger(access.getSerialNumber()), notBefore, notAfter, subject, cPublicKey);

            String[] alternative = super.access.getAlternativeName(Constants.SAN);
            GeneralName newGenName = null;
            int j = 0;
            GeneralName[] genName = new GeneralName[alternative.length];
            for (int i = 0; i < alternative.length; i++) {
                String[] array = alternative[i].split("=");
                switch (array[0]) {
                    case "rfc822Name":
                        newGenName = new GeneralName(GeneralName.rfc822Name, array[1]);
                        genName[j++] = newGenName;
                        break;

                    case "dNSName":
                        newGenName = new GeneralName(GeneralName.dNSName, array[1]);
                        genName[j++] = newGenName;
                        break;

                    case "ediPartyName":
                        newGenName = new GeneralName(GeneralName.ediPartyName, array[1]);
                        genName[j++] = newGenName;
                        break;

                    case "iPAddress":
                        newGenName = new GeneralName(GeneralName.iPAddress, array[1]);
                        genName[j++] = newGenName;
                        break;

                    case "otherName":
                        newGenName = new GeneralName(GeneralName.otherName, array[1]);
                        genName[j++] = newGenName;
                        break;

                    case "registeredID":
                        newGenName = new GeneralName(GeneralName.registeredID, array[1]);
                        genName[j++] = newGenName;
                        break;

                    case "x400Address":
                        newGenName = new GeneralName(GeneralName.x400Address, array[1]);
                        genName[j++] = newGenName;
                        break;

                    case "directoryName":
                        newGenName = new GeneralName(GeneralName.directoryName, array[1]);
                        genName[i++] = newGenName;
                        break;

                    case "uniformResourceIdentifier":
                        newGenName = new GeneralName(GeneralName.uniformResourceIdentifier, array[1]);
                        genName[j++] = newGenName;
                        break;
                }

            }

            GeneralName[] genNameTemp = new GeneralName[j];
            System.arraycopy(genName, 0, genNameTemp, 0, genNameTemp.length);
            genName = genNameTemp;

            GeneralNames genNames = new GeneralNames(genName);
            if (alternative.length > 0) {
                builder.addExtension(Extension.subjectAlternativeName, access.isCritical(Constants.SAN), genNames);
            }

            if (access.getInhibitAnyPolicy()) {

                builder.addExtension(Extension.inhibitAnyPolicy, access.isCritical(Constants.IAP), new ASN1Integer(Integer.parseInt(access.getSkipCerts())));

            }

            if (access.getAnyPolicy()) {

                PolicyQualifierInfo policyQualifierInfo = new PolicyQualifierInfo(access.getCpsUri());
                PolicyInformation policyInformation = new PolicyInformation(PolicyQualifierId.id_qt_cps, new DERSequence(policyQualifierInfo));
                CertificatePolicies certificatePolicies = new CertificatePolicies(policyInformation);

                builder.addExtension(Extension.certificatePolicies, access.isCritical(Constants.CP), certificatePolicies);
            }

            ContentSigner contentSigner = new JcaContentSignerBuilder(access.getPublicKeyDigestAlgorithm()).build(cPrivateKey);

            X509Certificate certificate = new JcaX509CertificateConverter().setProvider(BouncyCastleProvider.PROVIDER_NAME).getCertificate(builder.build(contentSigner));

            certificate.verify(cPublicKey);
            System.out.println(certificate);
            X509Certificate[] chain = new X509Certificate[1];
            chain[0] = certificate;
            keyStore.setKeyEntry(string, cPrivateKey, MyCode.KEY_STORE_PASSWORD, chain);

            try (OutputStream fos = new FileOutputStream(new File(MyCode.KSTORE_FILE))) {
                keyStore.store(fos, KEY_STORE_PASSWORD);
            }

            return true;

        } catch (KeyStoreException | IOException | NoSuchAlgorithmException | CertificateException | NoSuchProviderException | InvalidAlgorithmParameterException | OperatorCreationException | InvalidKeyException | SignatureException ex) {
            Logger.getLogger(MyCode.class.getName()).log(Level.SEVERE, null, ex);
        }

        return false;
    }

    @Override
    public boolean importCertificate(String string, String string1) {
        System.out.println("importCertificate " + string + " " + string1);

        try {

            KeyStore keyStore = KeyStore.getInstance(KEY_STORE_PKCS12, BouncyCastleProvider.PROVIDER_NAME);

            try (InputStream is = new FileInputStream(new File(KSTORE_FILE))) {
                keyStore.load(is, KEY_STORE_PASSWORD);
            } catch (IOException ex) {
                Logger.getLogger(MyCode.class.getName()).log(Level.SEVERE, null, ex);
                keyStore.load(null, null);
            }

            try (InputStream is = new FileInputStream(new File(string));
                    BufferedInputStream bis = new BufferedInputStream(is)) {
                /////////////////////////////////////////////////////////////////////////////////////////
                //test////////////////
                /**
                 * @TODO test CertificateFactory
                 */
                Certificate certificate = CertificateFactory.getInstance(X509, BouncyCastleProvider.PROVIDER_NAME).generateCertificate(bis);

                //Certificate certificate = new CertificateFactory().engineGenerateCertificate(bis);
                ////////////////////////////////////////////////////////////////////////////////////////////
                keyStore.setCertificateEntry(string1, certificate);

                try (OutputStream fos = new FileOutputStream(new File(MyCode.KSTORE_FILE))) {
                    keyStore.store(fos, KEY_STORE_PASSWORD);
                }

                return true;
            }

        } catch (KeyStoreException | IOException | NoSuchAlgorithmException | CertificateException | NoSuchProviderException ex) {
            Logger.getLogger(MyCode.class.getName()).log(Level.SEVERE, null, ex);
        }

        return false;
    }

    //https://www.programcreek.com/java-api-examples/?api=org.bouncycastle.util.io.pem.PemWriter
    @Override
    public boolean exportCertificate(String string, String string1,
            int i, int i1
    ) {
        System.out.println("exportCertificate " + string + " " + string1 + " " + i + " " + i1);
        int len = string.length();
        if (len > 4 && string.charAt(len - 1) == 'r'
                && string.charAt(len - 2) == 'e'
                && string.charAt(len - 3) == 'c'
                && string.charAt(len - 4) == '.') {

            try {

                KeyStore keyStore = KeyStore.getInstance(KEY_STORE_PKCS12, BouncyCastleProvider.PROVIDER_NAME);

                try (InputStream is = new FileInputStream(new File(KSTORE_FILE))) {
                    keyStore.load(is, KEY_STORE_PASSWORD);
                } catch (IOException ex) {
                    Logger.getLogger(MyCode.class.getName()).log(Level.SEVERE, null, ex);
                    keyStore.load(null, null);
                }

                if (i == 0) {
                    try (OutputStream os = new FileOutputStream(new File(string))) {
                        X509Certificate certificate = (X509Certificate) keyStore.getCertificate(string1);
                        os.write(certificate.getEncoded());
                    }
                } else if (i == 1) {
                    try (/*FileWriter fw = new FileWriter(new File(string));*/
                            OutputStream os = new FileOutputStream(new File(string));
                            JcaPEMWriter writer = new JcaPEMWriter(new OutputStreamWriter(os))) {
                        if (i1 == 0) {
                            Certificate certificate = keyStore.getCertificate(string1);
                            writer.writeObject(certificate);
                        } else if (i1 == 1) {
                            Certificate[] certificates = keyStore.getCertificateChain(string1);
                            for (Certificate cert : certificates) {
                                writer.writeObject(cert);
                            }

                        }
                    }
                }

                return true;

            } catch (KeyStoreException | IOException | NoSuchAlgorithmException | CertificateException | NoSuchProviderException ex) {
                Logger.getLogger(MyCode.class.getName()).log(Level.SEVERE, null, ex);
            }

        }

        return false;
    }

    @Override
    public boolean exportCSR(String string, String string1, String string2) {
        System.out.println("exportCSR " + string + " " + string1 + string2);

        try (FileOutputStream fos = new FileOutputStream(new File(string))) {

            KeyStore keyStore = KeyStore.getInstance(KEY_STORE_PKCS12, BouncyCastleProvider.PROVIDER_NAME);

            try (FileInputStream is = new FileInputStream(new File(KSTORE_FILE))) {
                keyStore.load(is, KEY_STORE_PASSWORD);
            } catch (IOException ex) {
                Logger.getLogger(MyCode.class.getName()).log(Level.SEVERE, null, ex);
                keyStore.load(null, null);
            }

            X509Certificate certificate = (X509Certificate) keyStore.getCertificate(string1);
            X509CertificateHolder certificateHolder = new JcaX509CertificateHolder(certificate);
            List<ASN1ObjectIdentifier> aSN1ObjectIdentifiers = certificateHolder.getExtensionOIDs();
            ExtensionsGenerator generator = new ExtensionsGenerator();
            for (ASN1ObjectIdentifier aSN1ObjectIdentifier : aSN1ObjectIdentifiers) {
                generator.addExtension(certificateHolder.getExtension(aSN1ObjectIdentifier));
            }
            X500Name name = new JcaX509CertificateHolder(certificate).getSubject();
            PKCS10CertificationRequestBuilder requestBuilder = new JcaPKCS10CertificationRequestBuilder(name, certificate.getPublicKey());

            requestBuilder.addAttribute(PKCSObjectIdentifiers.pkcs_9_at_extensionRequest, generator.generate());

            JcaContentSignerBuilder signerBuilder = new JcaContentSignerBuilder(string2);
            PKCS10CertificationRequest request = requestBuilder.build(signerBuilder.build((PrivateKey) keyStore.getKey(string1, KEY_STORE_PASSWORD)));

            fos.write(request.getEncoded());
            
            return true;
        } catch (KeyStoreException | NoSuchAlgorithmException | CertificateException | IOException | OperatorCreationException | UnrecoverableKeyException | NoSuchProviderException ex) {
            Logger.getLogger(MyCode.class.getName()).log(Level.SEVERE, null, ex);
        }

        return false;
    }

    @Override
    public String importCSR(String string) {
        System.out.println("importCSR " + string);
        File f = new File(string);
        if (f.exists()) {
            try (FileInputStream is = new FileInputStream(f)) {

                byte[] data = new byte[(int) f.length()];
                is.read(data);
                csr = new JcaPKCS10CertificationRequest(data);

                return csr.getSubject().toString();

            } catch (IOException ex) {
                Logger.getLogger(MyCode.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
        return null;
    }

    //https://github.com/gurongfang/BouncyCastleSample/blob/master/src/GenerateSignedCertificate.java
    @Override
    public boolean signCSR(String string, String string1, String string2) {
        System.out.println("signCSR " + string + " " + string1 + string2);

        if (csr != null) {

            try (FileOutputStream fos = new FileOutputStream(new File(string));
                    /*ByteArrayOutputStream out = new ByteArrayOutputStream();*/
                    JcaPEMWriter pemWriter = new JcaPEMWriter(new OutputStreamWriter(fos))) {

                KeyStore keyStore = KeyStore.getInstance(KEY_STORE_PKCS12, BouncyCastleProvider.PROVIDER_NAME);

                try (FileInputStream is = new FileInputStream(new File(KSTORE_FILE))) {
                    keyStore.load(is, KEY_STORE_PASSWORD);
                } catch (IOException ex) {
                    Logger.getLogger(MyCode.class.getName()).log(Level.SEVERE, null, ex);
                    keyStore.load(null, null);
                }

                PrivateKey privateKey = (PrivateKey) keyStore.getKey(string1, KEY_STORE_PASSWORD);
                X509Certificate certificate = (X509Certificate) keyStore.getCertificate(string1);

                JcaX509CertificateHolder certificateHolder = new JcaX509CertificateHolder(certificate);

                Date notBefore = access.getNotBefore();
                Date notAfter = access.getNotAfter();
                BigInteger serialNumber = new BigInteger(access.getSerialNumber());

                JcaX509v3CertificateBuilder builder = new JcaX509v3CertificateBuilder(
                        certificateHolder.getIssuer(),
                        serialNumber,
                        notBefore,
                        notAfter,
                        csr.getSubject(),
                        csr.getPublicKey());

                String[] alternative = super.access.getAlternativeName(Constants.SAN);
                if (alternative.length > 0) {
                    GeneralName newGenName = null;
                    GeneralName[] genName = new GeneralName[alternative.length];
                    for (int i = 0; i < alternative.length; i++) {
                        String[] array = alternative[i].split("=");
                        switch (array[0]) {
                            case "rfc822Name":
                                newGenName = new GeneralName(GeneralName.rfc822Name, array[1]);
                                genName[i] = newGenName;
                                break;

                            case "dNSName":
                                newGenName = new GeneralName(GeneralName.dNSName, array[1]);
                                genName[i] = newGenName;
                                break;

                            case "ediPartyName":
                                newGenName = new GeneralName(GeneralName.ediPartyName, array[1]);
                                genName[i] = newGenName;
                                break;

                            case "iPAddress":
                                newGenName = new GeneralName(GeneralName.iPAddress, array[1]);
                                genName[i] = newGenName;
                                break;

                            case "otherName":
                                newGenName = new GeneralName(GeneralName.otherName, array[1]);
                                genName[i] = newGenName;
                                break;

                            case "registeredID":
                                newGenName = new GeneralName(GeneralName.registeredID, array[1]);
                                genName[i] = newGenName;
                                break;

                            case "x400Address":
                                newGenName = new GeneralName(GeneralName.x400Address, array[1]);
                                genName[i] = newGenName;
                                break;

                            case "directoryName":
                                newGenName = new GeneralName(GeneralName.directoryName, array[1]);
                                genName[i] = newGenName;
                                break;

                            case "uniformResourceIdentifier":
                                newGenName = new GeneralName(GeneralName.uniformResourceIdentifier, array[1]);
                                genName[i] = newGenName;
                                break;
                        }

                    }
                    GeneralNames genNames = new GeneralNames(genName);
                    builder.addExtension(Extension.subjectAlternativeName, access.isCritical(Constants.SAN), genNames);

                }
                if (access.getInhibitAnyPolicy()) {
                    builder.addExtension(Extension.inhibitAnyPolicy, access.isCritical(Constants.IAP), new ASN1Integer(Integer.parseInt(super.access.getSkipCerts())));
                    //builder.addExtension(Extension.inhibitAnyPolicy, true, new ASN1Integer(Integer.parseInt(super.access.getSkipCerts())));

                }

                if (access.getAnyPolicy()) {
                    PolicyQualifierInfo policyQualifierInfo = new PolicyQualifierInfo(access.getCpsUri());
                    PolicyInformation policyInformation = new PolicyInformation(PolicyQualifierId.id_qt_cps, new DERSequence(policyQualifierInfo));
                    CertificatePolicies certificatePolicies = new CertificatePolicies(policyInformation);

                    builder.addExtension(Extension.certificatePolicies, access.isCritical(Constants.CP), certificatePolicies);
                    //builder.addExtension(Extension.certificatePolicies, true, certificatePolicies);
                }

                JcaContentSignerBuilder signerBuilder = new JcaContentSignerBuilder(string2);

                ContentSigner signer = signerBuilder.setProvider(BouncyCastleProvider.PROVIDER_NAME).build(privateKey);
//                CMSSignedDataGenerator generator = new CMSSignedDataGenerator();
//                generator.addSignerInfoGenerator(new SignerInfoGeneratorBuilder(new JcaDigestCalculatorProviderBuilder().build()).build(signer, certificateHolder));
                X509CertificateHolder certHolder = builder.build(signer);
//                byte[] certencoded = certHolder.toASN1Structure().getEncoded();
                //JcaCertStore jcaCertStore = new JcaCertStore(Arrays.asList(new Certificate[] {new JcaX509CertificateConverter().getCertificate(certHolder),certificate} ));
//                generator.addCertificate(new X509CertificateHolder(certencoded));
//                generator.addCertificate(new JcaX509CertificateHolder(certificate));
                //generator.addCertificates(jcaCertStore);
//                CMSTypedData typedData = new CMSProcessableByteArray(certencoded);
//                CMSSignedData signedData = generator.generate(typedData, false);

//                out.write("-----BEGIN PKCS #7 SIGNED DATA-----\n".getBytes("ISO-8859-1"));
//                out.write(Base64.encode(signedData.getEncoded()));
//                out.write("\n-----END PKCS #7 SIGNED DATA-----\n".getBytes("ISO-8859-1"));
                //fos.write(out.toByteArray());
                JcaX509CertificateConverter converter = new JcaX509CertificateConverter();
                X509Certificate genCertificate = converter.getCertificate(certHolder);
                pemWriter.writeObject(genCertificate);
                //pemWriter.writeObject(certificate);
                Certificate[] certificates = keyStore.getCertificateChain(string1);
                for (Certificate c : certificates) {
                    pemWriter.writeObject(c);
                }

                return true;
            } catch (KeyStoreException | NoSuchAlgorithmException | CertificateException | IOException | UnrecoverableKeyException | OperatorCreationException | NoSuchProviderException | InvalidKeyException ex) {
                Logger.getLogger(MyCode.class.getName()).log(Level.SEVERE, null, ex);
            }

        }

        return false;
    }

	//https://stackoverflow.com/questions/31118893/how-to-load-pkcs7-p7b-file-in-java
	//https://chengzhuliulin.wordpress.com/2013/08/09/create-pkcs12-from-pem-private-key-file-and-pkcs7-certificate-in-java/
    @Override
    public boolean importCAReply(String string, String string1) {
        System.out.println("importCAReply " + string + " " + string1);

        try (FileInputStream fis = new FileInputStream(new File(string))) {

            KeyStore keyStore = KeyStore.getInstance(KEY_STORE_PKCS12, BouncyCastleProvider.PROVIDER_NAME);

            try (FileInputStream is = new FileInputStream(new File(KSTORE_FILE))) {
                keyStore.load(is, KEY_STORE_PASSWORD);
            } catch (IOException ex) {
                Logger.getLogger(MyCode.class.getName()).log(Level.SEVERE, null, ex);
                keyStore.load(null, null);
            }
            /////////////////////////////////////////////////////////////////////////////////////
            //test/////////////////////////////////////////////////////////////////////////
            /**
             * @TODO test CertificateFactory
             */
            CertificateFactory certificateFactory = CertificateFactory.getInstance(X509, BouncyCastleProvider.PROVIDER_NAME);
            //CertificateFactory certificateFactory = new CertificateFactory();
            /**
             * @TODO Object or ? extends Certificate
             */
            //Collection<Object> collection = certificateFactory.engineGenerateCertificates(fis);
            Collection<Certificate> collection = (Collection<Certificate>) certificateFactory.generateCertificates(fis);
            //////////////////////////////////////////////////////////////////////////////

            //https://stackoverflow.com/questions/3293946/the-easiest-way-to-transform-collection-to-array
            Certificate[] certificates = collection.toArray(new Certificate[collection.size()]);

            PrivateKey privateKey = (PrivateKey) keyStore.getKey(string1, KEY_STORE_PASSWORD);
            keyStore.setKeyEntry(string1, privateKey, KEY_STORE_PASSWORD, certificates);

            try (OutputStream fos = new FileOutputStream(new File(MyCode.KSTORE_FILE))) {
                keyStore.store(fos, KEY_STORE_PASSWORD);
            }

            return true;

        } catch (KeyStoreException | IOException | NoSuchAlgorithmException | CertificateException | NoSuchProviderException | UnrecoverableKeyException ex) {
            Logger.getLogger(MyCode.class.getName()).log(Level.SEVERE, null, ex);
        }

        return false;

    }

    @Override
    public boolean canSign(String string) {
        System.out.println("canSign " + string);
        try {

            KeyStore keyStore = KeyStore.getInstance(KEY_STORE_PKCS12, BouncyCastleProvider.PROVIDER_NAME);

            try (InputStream is = new FileInputStream(new File(KSTORE_FILE))) {
                keyStore.load(is, KEY_STORE_PASSWORD);
            } catch (IOException ex) {
                Logger.getLogger(MyCode.class.getName()).log(Level.SEVERE, null, ex);
                keyStore.load(null, null);
            }

            X509Certificate certificate = (X509Certificate) keyStore.getCertificate(string);

            if (certificate != null) {
                if (certificate.getKeyUsage() != null && certificate.getKeyUsage()[5]) {
                    return true;
                } else if (certificate.getBasicConstraints() != -1) {
                    return true;
                }
            }

        } catch (KeyStoreException | IOException | NoSuchAlgorithmException | CertificateException | NoSuchProviderException ex) {
            Logger.getLogger(MyCode.class.getName()).log(Level.SEVERE, null, ex);
        }

        return false;
    }

    @Override
    public String getSubjectInfo(String string) {
        System.out.println("getSubjectInfo " + string);

        try {

            KeyStore keyStore = KeyStore.getInstance(KEY_STORE_PKCS12, BouncyCastleProvider.PROVIDER_NAME);

            try (InputStream is = new FileInputStream(new File(KSTORE_FILE))) {
                keyStore.load(is, KEY_STORE_PASSWORD);
            } catch (IOException ex) {
                Logger.getLogger(MyCode.class.getName()).log(Level.SEVERE, null, ex);
                keyStore.load(null, null);
            }

            X509Certificate certificate = (X509Certificate) keyStore.getCertificate(string);

            if (certificate != null) {
                X500Name name = new JcaX509CertificateHolder(certificate).getSubject();
                return name.toString();
            }

        } catch (KeyStoreException | IOException | NoSuchAlgorithmException | CertificateException | NoSuchProviderException ex) {
            Logger.getLogger(MyCode.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }

    @Override
    public String getCertPublicKeyAlgorithm(String string) {
        System.out.println("getCertPublicKeyAlgorithm " + string);

        try {

            KeyStore keyStore = KeyStore.getInstance(KEY_STORE_PKCS12, BouncyCastleProvider.PROVIDER_NAME);

            try (InputStream is = new FileInputStream(new File(KSTORE_FILE))) {
                keyStore.load(is, KEY_STORE_PASSWORD);
            } catch (IOException ex) {
                Logger.getLogger(MyCode.class.getName()).log(Level.SEVERE, null, ex);
                keyStore.load(null, null);
            }

            X509Certificate certificate = (X509Certificate) keyStore.getCertificate(string);

            if (certificate != null) {
                String alg = certificate.getPublicKey().getAlgorithm();
                return alg;
            }

        } catch (KeyStoreException | IOException | NoSuchAlgorithmException | CertificateException | NoSuchProviderException ex) {
            Logger.getLogger(MyCode.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }

    @Override
    public String getCertPublicKeyParameter(String string) {
        System.out.println("getCertPublicKeyParameter " + string);

        try {

            KeyStore keyStore = KeyStore.getInstance(KEY_STORE_PKCS12, BouncyCastleProvider.PROVIDER_NAME);

            try (InputStream is = new FileInputStream(new File(KSTORE_FILE))) {
                keyStore.load(is, KEY_STORE_PASSWORD);
            } catch (IOException ex) {
                Logger.getLogger(MyCode.class.getName()).log(Level.SEVERE, null, ex);
                keyStore.load(null, null);
            }

            X509Certificate certificate = (X509Certificate) keyStore.getCertificate(string);

            String alg = certificate.getPublicKey().getAlgorithm();

            if (alg.contains(RSA)) {

                RSAPublicKey aPublicKey = (RSAPublicKey) certificate.getPublicKey();
                int len = aPublicKey.getModulus().bitLength();
                return "" + len;
            } else if (alg.contains(DSA)) {
                DSAPublicKey aPublicKey = (DSAPublicKey) certificate.getPublicKey();
                int len = aPublicKey.getParams().getP().bitLength();
                return "" + len;
            } else if (alg.contains(EC)) {
                ECPublicKey cPublicKey = (ECPublicKey) certificate.getPublicKey();
                return cPublicKey.getParams().getCurve().toString();
            }

        } catch (KeyStoreException | IOException | NoSuchAlgorithmException | CertificateException | NoSuchProviderException ex) {
            Logger.getLogger(MyCode.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }

}
